import React from 'react';

import { ButtonVariant } from './ButtonVariant';

export default {
  title: 'Example/ButtonVariant',
  component: ButtonVariant,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

const Template = (args) => <ButtonVariant {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'ButtonVariant',
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'ButtonVariant',
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  label: 'ButtonVariant',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  label: 'ButtonVariant',
};
